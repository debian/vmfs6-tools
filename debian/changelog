vmfs6-tools (0.2.1-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 13 Oct 2022 08:46:45 -0000

vmfs6-tools (0.2.1-1) unstable; urgency=medium

  * New upstream tag, track this to fix package variances due to
    GH changes that were introduced in 0.2.0-3 and by MS Upstream.

 -- Thomas Ward <teward@ubuntu.com>  Wed, 12 Oct 2022 21:28:38 -0400

vmfs6-tools (0.2.0-3) unstable; urgency=medium

  * d/watch: Major updates to watchfile to work around the GitHub
    UI changes (see ML thread at
    https://lists.debian.org/debian-devel/2022/09/msg00224.html),
    by scraping/searching GitHub API responses for the equivalent
    releases data page.

 -- Thomas Ward <teward@ubuntu.com>  Wed, 05 Oct 2022 16:22:26 -0400

vmfs6-tools (0.2.0-2) unstable; urgency=medium

  * Trim trailing whitespace.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.
  * Remove Section on vmfs6-tools that duplicates source.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 10 Sep 2021 18:55:33 -0000

vmfs6-tools (0.2.0-1) unstable; urgency=medium

  * Apply new code changes for new VMFS Info magic from weafon/vmfs6-tool
  * New release (0.2.0).

 -- Thomas Ward <teward@ubuntu.com>  Tue, 05 Apr 2022 13:01:24 -0400

vmfs6-tools (0.1.0-3) unstable; urgency=medium

  * d/control:
    - Add Homepage and Vcs-{Browser,Git} fields, now that we're on Salsa and
      the package is using a git-buildpackage workflow.

 -- Thomas Ward <teward@ubuntu.com>  Sun, 29 Dec 2019 18:54:06 -0500

vmfs6-tools (0.1.0-2) unstable; urgency=medium

  * d/control:
    - Reorder build dependencies to be more readable.
    - Bump standards version to 4.4.1 (latest)

 -- Thomas Ward <teward@ubuntu.com>  Sat, 28 Dec 2019 22:59:56 -0500

vmfs6-tools (0.1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #939600)
  * Initial packaging based on packaging from vmfs-tools 0.2.5-1.

 -- Thomas Ward <teward@ubuntu.com>  Fri, 06 Sep 2019 12:32:59 -0400
